from django.contrib import admin
from django.urls import path,include
from .views import index,cv,time

urlpatterns = [
    path('',index, name='index'),
    path('cv/',cv, name='cv'),
    path('time/',time, name='time')
]
