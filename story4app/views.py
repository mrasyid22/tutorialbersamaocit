from django.http import HttpResponse
from django.shortcuts import render
import datetime
from django.utils import timezone

# Create your views here.
def index(request):
    return render(request, "index.html")

def cv(request):
    return render(request, "cv.html")

def time(request):
    return render(request, "time.html", {'time': timezone.now()})





