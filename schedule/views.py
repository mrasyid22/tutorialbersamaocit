from django.shortcuts import render, redirect
from .models import Schedule, Friend
from .forms import ScheduleForm, FriendForm
import datetime

# Create your views here.
def schedule_index(request, *args, **kwargs):
    my_form = ScheduleForm(Schedule)
    schedules = Schedule.objects.all()

    context = {
        'form' : my_form,
        'schedules' : schedules
    }
    return render(request, "schedule.html", context)  

def schedule_detail(request, id):
    schedule = Schedule.objects.get(id=id)
    context = {"schedule": schedule}
    return render(request, 'schedule_detail.html', context)

def schedule_create(request):
    my_form = ScheduleForm()

    if request.method == 'POST':
        my_form = ScheduleForm(request.POST)
        if my_form.is_valid():
            Schedule.objects.create(**my_form.cleaned_data)
            return redirect("schedule")

    context = {
        'form' : my_form,
    }
    return render(request, "schedule_create.html", context)  

def remove_schedule(request, id, *args, **kwargs):
    Schedule.objects.filter(id=id).delete()

    return redirect("schedule")

def friend_index(request, *args, **kwargs):
    form = FriendForm(Friend)
    friends = Friend.objects.all()

    context = {
        'form' : form,
        'friends' : friends
    }
    return render(request, "friend.html", context)

def friend_detail(request, id):
    friend = Friend.objects.get(id=id)
    context = {"friend": friend}
    return render(request, 'friend_detail.html', context)

def remove_friend(request, id):
    Friend.objects.filter(id=id).delete()

    return redirect("friend")

def new_friend(request):
    friend_form = FriendForm()

    if request.method == 'POST':
        friend_form = FriendForm(request.POST)
        if friend_form.is_valid():
            Friend.objects.create(**friend_form.cleaned_data)
            return redirect("friend")

    context = {
        'form' : friend_form,
    }
    return render(request, "new_friend.html", context)  