from django.db import models

# Create your models here.

class Schedule(models.Model) :
    kelas = models.CharField(max_length=80)
    dosen = models.CharField(max_length=40)
    jumlahSks = models.IntegerField()
    deskripsi = models.TextField(max_length=100)
    tempat = models.CharField(max_length=40)

    GANJIL = "Ganjil 2019/2020"
    GENAP = "Genap 2019/2020"
    PilihanTahun = [(GENAP, 'Genap 2019/2020'), (GANJIL, 'Ganjil 2019/2020')]
    tahun = models.CharField(max_length=16, choices=PilihanTahun, default="GENAP")

class Friend(models.Model):
    nama = models.CharField(max_length=80)
    umur = models.IntegerField()

    Cowo = 'Laki-Laki'
    Cewe = 'Perempuan'
    pilihanGender = [(Cowo, 'Laki-Laki'),(Cewe, 'Perempuan')]
    gender = models.CharField(max_length=16, choices=pilihanGender)


