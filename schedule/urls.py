from django.contrib import admin
from django.urls import path
from .views import *

urlpatterns = [
    path('schedule/', schedule_index, name='schedule' ),
    path('schedule-create/', schedule_create, name='scheduleCreate' ),
    path('schedule-detail/<int:id>', schedule_detail, name='scheduleDetail'),
    path('schedule-delete/<int:id>', remove_schedule, name="remove_schedule"),
    path('friend/', friend_index, name='friend'),
    path('friend/<int:id>', friend_detail, name='friendDetail'),
    path('friend-delete/<int:id>', remove_friend, name='remove_friend'),
    path('new-friend/', new_friend, name='new_friend')
]

