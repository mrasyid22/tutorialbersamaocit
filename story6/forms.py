from django import forms

class KegiatanForm(forms.Form):
    namaKegiatan = forms.CharField(label='Nama Kegiatan', max_length=64,widget=forms.TextInput(attrs={'class':'form-control', 'placeholder' : 'Masukkan nama kegiatan...', 'style': 'width:40vw'}))

class PesertaForm(forms.Form):
    namaPeserta = forms.CharField(label='Nama Peserta', max_length=64, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder' : 'Masukkan namamu...'}))
